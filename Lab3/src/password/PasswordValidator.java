package password;

//Ryan Warnakula 991464437

public class PasswordValidator {
	public static void main(String[] args){
		//TODO
	}
	
	//Check the password length is at least 8
	public static boolean checkPasswordLength(String password){
		//If the length is equal to 8 or greater, then it is true. if otherwise, it's false
		if( password.length() >= 8 ) {
			return true;
		}else {
			return false;
		}
	}
	
	//Check if the string contains atleast 2 digits
	public static boolean containsDigits(String password){
		int count = 0; //Count of Digits
		//Check each character in string and if the char is a digit, then increase count
		for (int i = 0; i < password.length(); i++) {
		    if (Character.isDigit(password.charAt(i))) {
		        count++;
		        System.out.println(count);
		    }
		}
		
		//if the password does contain 2 digits atleast, then return true. Otherwise 
		//return false  
		if(count >= 2){
			return true;
		}
		return false;
	}
		
}
