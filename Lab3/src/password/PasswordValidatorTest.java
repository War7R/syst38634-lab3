package password;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

//Ryan Warnakula 991464437

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	//random normal string
	@Test
	public void testCheckPasswordLength(){
	assertTrue("Invalid Password Length",
				PasswordValidator.checkPasswordLength("thisisvalidpassword"));
	}
	
	//if the string is null
	@Test (expected = NullPointerException.class)
	public void testCheckPasswordLengthException(){
	assertTrue("Invalid Password Length",
				PasswordValidator.checkPasswordLength(null));
	}
	
	//If the password length is 8
	@Test 
	public void testCheckPasswordLengthBoundaryIn(){
	assertTrue("Invalid Password Length",
				PasswordValidator.checkPasswordLength("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP"));
	}
	
	//If the password length is 7
	@Test 
	public void testCheckPasswordLengthBoundaryOut(){
	assertTrue("Invalid Password Length",
				PasswordValidator.checkPasswordLength("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP"));
	}
	
	//contains two or more digits
	@Test
	public void containsDigits(){
		assertTrue("Password does not contain at least two digits.",
				PasswordValidator.containsDigits("thisis112233"));
	}
	
	//If string is null
	@Test (expected = NullPointerException.class)
	public void containsDigitsException(){
		assertTrue("Password does not contain at least two digits.",
				PasswordValidator.containsDigits(null));
	}
	
	//Contains one digit
	@Test
	public void containsDigitsBoundaryOut(){
		assertTrue("Password does not contain one or less digits.",
				PasswordValidator.containsDigits("thisisvalidpassword2") != true);
	}
	
	//Contains two digits
	@Test
	public void containsDigitsBoundaryIn(){
		assertTrue("Password does not contain at least two digits.",
				PasswordValidator.containsDigits("thisisvalidpassword22"));
	}

}
